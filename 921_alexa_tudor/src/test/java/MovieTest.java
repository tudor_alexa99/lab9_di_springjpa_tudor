import model.Movie;

public class MovieTest {
    private Movie movie;
    private Movie movie2;

    public void setUp(){
        this.movie = new Movie("Test",100,2000);
        this.movie2 = new Movie("Test2",100,2000);
    }
    public void testGetTitle(){
        assert movie.getTitle().equals("Test");
    }
    public void testGetDuration(){
        assert movie.getDuration() == 100;
    }
    public void testGetYear(){
        assert movie.getYear() == 2000;
    }
    public void testSetTitle(){
        movie2.setTitle("NewTitle");
        assert movie2.getTitle().equals("NewTitle");
    }
    public void testSetDuration(){
        movie2.setDuration(666);
        assert movie2.getDuration() == 666;
    }
    public void testSetYear(){
        movie2.setYear(2020);
        assert movie2.getYear() == 2020;
    }
}

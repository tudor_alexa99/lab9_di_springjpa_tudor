import model.Client;

public class ClientTest {
    private Client c;
    private Client c2;

    public void setUp() {
        this.c = new Client("Name", "mail@mail.com");
        this.c = new Client("Name2", "mail@mail.com");
    }

    public void testGetName() {
        assert this.c.getName().equals("Name");
    }

    public void testSetEmail() {
        assert this.c.getEmail().equals("mail@mail.com");
    }

    public void testSetName() {
        this.c2.setName("NewName");
        assert this.c2.getName().equals("NewName");
    }

    public void testGetEmail() {
        this.c2.setEmail("NewEmail");
        assert this.c2.getEmail().equals("NewEmail");
    }
}
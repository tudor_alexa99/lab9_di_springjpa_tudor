package repository;

import model.BaseClass;
import model.validators.Validator;
import model.exceptions.ValidatorException;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryRepository <ID, T extends BaseClass<ID>> implements IRepository<ID, T>{
    private Map<ID, T> entities;
    private Validator<T> validator;

    public InMemoryRepository( Validator<T> validator) {
        this.entities = new HashMap<>();
        this.validator = validator;
    }

    @Override
    public Optional<T> findOne(ID id) {
        checkNull(id, "Id"); //If this thing does not work, it will throw an error anyways
        return Optional.ofNullable(entities.get(id));
    }

    @Override
    public Iterable<T> findAll() {
        return this.entities.values()
                .stream()
                .sorted(Comparator.comparingLong(e -> (Long) e.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<T> save(T entity) throws ValidatorException {
        checkNull(entity, "Entity");
        validator.validate(entity);

        return Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));
    }

    @Override
    public Optional<T> delete(ID id) {
        checkNull(id, "ID");
        return Optional.ofNullable(this.entities.remove(id));
    }

    @Override
    public Optional<T> update(T entity) throws ValidatorException {
        checkNull(entity, "Entity");
        validator.validate(entity);
        return Optional.ofNullable(entities.computeIfPresent(entity.getId(), (k,v) -> entity));
    }

    public static void checkNull(Object o, String message){
        if(o == null)
            throw new IllegalArgumentException(message +  " must not be null");
    }
}

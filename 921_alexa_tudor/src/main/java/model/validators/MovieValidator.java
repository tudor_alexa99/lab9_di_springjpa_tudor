package model.validators;

import model.Movie;
import model.exceptions.BaseException;
import model.exceptions.ValidatorException;

import java.util.Optional;

public class MovieValidator implements Validator<Movie> {
    @Override
    public void validate(Movie entity) throws ValidatorException {
        Optional.ofNullable(entity.getId()).orElseThrow( ()-> new BaseException("ID must not be null"));
        Optional.ofNullable(entity.getYear()).orElseThrow(() -> new BaseException("Title must not be null"));
        Optional.ofNullable(entity.getTitle()).orElseThrow(() -> new BaseException("Title must not be null"));
        Optional.ofNullable(entity.getDuration()).orElseThrow(() -> new BaseException("Title must not be null"));

        Optional.of(entity.getDuration())
                .filter((d) -> d <= 0)
                .ifPresent((d) -> {throw new BaseException("The duration cannot be <= 0");});
        Optional.of(entity.getTitle())
                .filter((t) -> t.matches(""))
                .ifPresent((t) -> {throw new BaseException("The title must not be empty!") ;});
    }
}

package model.validators;

import model.Rental;
import model.exceptions.BaseException;
import model.exceptions.ValidatorException;

import java.util.Optional;

public class RentalValidator implements Validator<Rental> {
    @Override
    public void validate(Rental entity) throws ValidatorException {
        Optional.ofNullable(entity.getId()).orElseThrow(() -> new BaseException("ID must not be null"));
        Optional.ofNullable(entity.getRentDate()).orElseThrow(() -> new BaseException("Rent date must not be null"));
    }
}

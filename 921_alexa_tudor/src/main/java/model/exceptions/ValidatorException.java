package model.exceptions;

public class ValidatorException extends BaseException {
    public ValidatorException(String message) {
        super(message);
    }
}

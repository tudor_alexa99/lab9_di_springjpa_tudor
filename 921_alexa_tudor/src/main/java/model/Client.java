package model;

public class Client extends BaseClass<Long> implements Comparable<Client> {
    private String name;
    private String email;
    public static long currId = 0;

    public Client(String name, String email) {
        this.name = name;
        this.email = email;
        this.setId(++currId);
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) { this.email = email; }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    @Override
    public String toString() {
        return getId() + ": " + getName() + " " + getEmail();
    }

    @Override
    public int compareTo(Client client) {
        return this.getId().compareTo(client.getId());
    }
}

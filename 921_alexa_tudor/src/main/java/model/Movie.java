package model;

public class Movie extends BaseClass<Long> implements Comparable<Movie> {
    private String title;
    private int duration;
    private int year;
    private static long currId = 0;

    public Movie(String title, int duration, int year) {
        this.title = title;
        this.duration = duration;
        this.year = year;
        this.setId(++currId);
    }

    public String getTitle() {
        return title;
    }

    public int getDuration() {
        return duration;
    }

    public int getYear() {
        return year;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return getId() + ": " + getTitle() + " (" + getYear() + ")";
    }

    @Override
    public int compareTo(Movie movie) {
        return this.getId().compareTo(movie.getId());
    }
}

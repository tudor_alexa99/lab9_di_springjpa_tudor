package model;

public class BaseClass<ID>  {
    private ID id;


    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BaseClass{" +
                "id=" + id +
                '}';
    }
}

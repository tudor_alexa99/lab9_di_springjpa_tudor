package ui;

import model.exceptions.BaseException;

import java.util.HashMap;
import java.util.Map;

public class Menu {
    private Map<String, Func> commands = new HashMap<>();
    private String header = "\n<---- Menu ----> \n";
    private String content = "";
    private String footer = "<-------------->";

    public void addCommand(String key, String description ,Func value){
        this.commands.put(key, value);
        this.content += key + ". " + description + '\n';
    }

    public void run(String comm) throws BaseException {
        try{
            this.commands.get(comm).exec();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public String getMenu() {
        return header + content + footer;
    }
}
